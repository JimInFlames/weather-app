package com.example.weathertest.di

import android.app.Application
import androidx.room.Room
import com.example.weathertest.data.db.AreaDao
import com.example.weathertest.data.db.AreaDatabase
import org.koin.dsl.module

/**
 *  DB module
 */

fun providesDatabase(application: Application): AreaDatabase =
    Room.databaseBuilder(application, AreaDatabase::class.java, "areadatabase")
        .build()
fun providesDao(db: AreaDatabase): AreaDao = db.areaDao()

val roomModule = module {
    single { providesDatabase(get()) }
    single { providesDao(get()) }
}