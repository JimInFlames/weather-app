package com.example.weathertest.di

import com.example.weathertest.network.api.ApiService
import com.example.weathertest.network.api.WeatherService
import com.example.weathertest.util.Endpoints
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Singleton instances of our network module
 */
val networkModule = module {
    single { retrofit() }
    single { apiService(get()) }
    single { createWeatherAppService(get()) }
}

fun createWeatherAppService(
    apiService: ApiService
): WeatherService = WeatherService(apiService)

fun apiService(retrofit: Retrofit): ApiService =
    retrofit.create(ApiService::class.java)

fun retrofit(): Retrofit = Retrofit.Builder()
    .baseUrl(Endpoints.BASE_URL)
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .addConverterFactory(GsonConverterFactory.create())
    .build()
