package com.example.weathertest.di

import com.example.weathertest.view.home.HomeAreasViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 *  Our View model module, for the dedicated screen
 */
val homeAreasViewModelModule = module {
    viewModel { HomeAreasViewModel(get()) }
}
