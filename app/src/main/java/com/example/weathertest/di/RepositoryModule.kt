package com.example.weathertest.di

import com.example.weathertest.data.db.AreaDao
import com.example.weathertest.data.repository.WeatherRepository
import com.example.weathertest.network.api.WeatherService
import org.koin.dsl.module

/**
 * Singleton repository module
 */
val repositoryModule = module {
    single { createRepository(get(), get()) }
}

fun createRepository(
    weatherService: WeatherService,
    areaDao: AreaDao,
): WeatherRepository = WeatherRepository(weatherService, areaDao)