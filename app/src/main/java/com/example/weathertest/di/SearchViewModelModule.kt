package com.example.weathertest.di

import com.example.weathertest.view.search.SearchAreaViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Our View model module, for the search screen
 */
val searchViewModelModule = module {
    viewModel { SearchAreaViewModel(get()) }
}