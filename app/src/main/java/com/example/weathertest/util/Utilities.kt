package com.example.weathertest.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Utilities {
    companion object {

        // Data used for the API
        const val PARSE_TYPE = "json"
        const val POPULAR = "yes"
        const val NUM_OF_DAYS = 5

        /**
         * Converts given time from Local Weather API to HH:mm format
         */
        fun getFormattedHour(hour: String): String? {
            if (hour == "1200")
                return "12:00"
            val inputPattern: String = when (hour.length) {
                3 -> {
                    "hmm"
                }
                4 -> {
                    "hhmm"
                }
                else -> {
                    "h"
                }
            }

            val outputPattern = "HH:mm"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)
            val date: Date?
            var str: String? = null
            try {
                date = inputFormat.parse(hour)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return str
        }

        /**
         * Given a date in yyyy-MM-dd format returns the name of the day
         */
        fun getNameDay(date: String): String {
            stringToDate(date).let {
                return SimpleDateFormat("EEEE").format(it)
            }
        }

        private fun stringToDate(dateString: String): Date? {
            val format = SimpleDateFormat("yyyy-MM-dd")
            var date: Date? = null
            try {
                date = format.parse(dateString)
                println(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return date
        }
    }
}