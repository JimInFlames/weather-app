package com.example.weathertest.util

object Endpoints {
    const val API_KEY = "2f1a16d0a3c442c1983113404211106"
    const val BASE_URL = "https://api.worldweatheronline.com/"
    const val SEARCH_URL = "premium/v1/search.ashx"
    const val DETAILS_URL = "premium/v1/weather.ashx"
}