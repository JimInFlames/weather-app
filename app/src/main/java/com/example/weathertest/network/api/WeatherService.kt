package com.example.weathertest.network.api

import com.example.weathertest.network.response.SearchApiResult
import com.example.weathertest.network.response.WeatherApiResult
import com.example.weathertest.util.Endpoints
import com.example.weathertest.util.Utilities

class WeatherService(private val apiService: ApiService) : BaseService() {
    suspend fun searchForAreas(searchQuery: String): Result<SearchApiResult> {
        return createCall {
            apiService.searchForCities(Endpoints.API_KEY,
                searchQuery,
                Utilities.PARSE_TYPE,
                Utilities.POPULAR)
        }
    }

    suspend fun getWeatherDetails(searchQuery: String): Result<WeatherApiResult> {
        return createCall {
            apiService.getWeatherDetails(Endpoints.API_KEY,
                searchQuery,
                Utilities.PARSE_TYPE,
                Utilities.NUM_OF_DAYS,
                1)
        }
    }
}