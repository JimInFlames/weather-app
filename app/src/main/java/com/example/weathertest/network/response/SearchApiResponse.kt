package com.example.weathertest.network.response

import com.example.weathertest.data.model.Area
import com.google.gson.annotations.SerializedName

/**
 * Response from the Search API
 */
data class SearchApiResult(
    @SerializedName("search_api")
    val results: SearchResults? = null,
)

data class SearchResults(
    @SerializedName("result")
    val areaList: List<Area>,
)


