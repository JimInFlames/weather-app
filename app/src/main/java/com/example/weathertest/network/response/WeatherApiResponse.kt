package com.example.weathertest.network.response

import com.example.weathertest.data.model.CurrentCondition
import com.example.weathertest.data.model.Weather
import com.google.gson.annotations.SerializedName

/**
 * Response from the details API
 */
data class WeatherApiResult(
    @SerializedName("data")
    val data: WeatherApiResultData? = null,
)

data class WeatherApiResultData(
    @SerializedName("current_condition")
    val currentCondition: List<CurrentCondition>? = null,
    @SerializedName("weather")
    val weather: List<Weather>? = null,
)


