package com.example.weathertest.network.api

import com.example.weathertest.network.response.SearchApiResult
import com.example.weathertest.network.response.WeatherApiResult
import com.example.weathertest.util.Endpoints
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET(Endpoints.SEARCH_URL)
    suspend fun searchForCities(
        @Query("key") api_key: String,
        @Query("query") searchQuery: String,
        @Query("format") format: String,
        @Query("popular") popular: String,
    ): Response<SearchApiResult>

    @GET(Endpoints.DETAILS_URL)
    suspend fun getWeatherDetails(
        @Query("key") api_key: String,
        @Query("q") searchQuery: String,
        @Query("format") format: String,
        @Query("num_of_days") popular: Int,
        @Query("tp") tp: Int,
    ): Response<WeatherApiResult>
}