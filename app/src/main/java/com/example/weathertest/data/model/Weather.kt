package com.example.weathertest.data.model

import com.google.gson.annotations.SerializedName

data class Weather(
    @SerializedName("date")
    val date: String,
    @SerializedName("avgtempC")
    val avgTempC: Int,
    @SerializedName("hourly")
    val hourlyData: List<HourlyData>,
)

data class HourlyData(
    @SerializedName("time")
    val time: Int,
    @SerializedName("tempC")
    val tempC: Int,
    @SerializedName("windspeedKmph")
    val windSpeedKmph: Int,
    @SerializedName("weatherIconUrl")
    val weatherIconUrl: List<EntityValue>,
    @SerializedName("weatherDesc")
    val weatherDesc: List<EntityValue>,
)
