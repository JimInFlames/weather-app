package com.example.weathertest.data.repository

import com.example.weathertest.data.db.AreaDao
import com.example.weathertest.data.model.Area
import com.example.weathertest.network.api.Result
import com.example.weathertest.network.api.WeatherService
import com.example.weathertest.network.response.SearchApiResult
import com.example.weathertest.network.response.WeatherApiResult
import kotlinx.coroutines.flow.Flow

class WeatherRepository(
    private val weatherService: WeatherService,
    private val areaDao: AreaDao,
) {

    suspend fun searchForAreas(searchQuery: String): SearchApiResult {
        return when (val result = weatherService.searchForAreas(searchQuery)) {
            is Result.Error -> SearchApiResult()
            is Result.Success -> result.data
            else -> SearchApiResult()
        }
    }

    suspend fun getWeatherDetails(searchQuery: String): WeatherApiResult {
        return when (val result = weatherService.getWeatherDetails(searchQuery)) {
            is Result.Error -> WeatherApiResult()
            is Result.Success -> result.data
            else -> WeatherApiResult()
        }
    }

    suspend fun insertArea(area: Area) {
        areaDao.insert(area)
    }

    suspend fun deleteArea(area: Area) {
        areaDao.delete(area)
    }

    fun getStoredAreas(): Flow<List<Area>> {
        return areaDao.getAllAreas()
    }
}