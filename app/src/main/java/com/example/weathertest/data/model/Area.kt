package com.example.weathertest.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.weathertest.data.db.Converters
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "area")
data class Area(
    @PrimaryKey
    @TypeConverters(Converters::class)
    @SerializedName("areaName")
    val areaName: List<EntityValue>,

    @SerializedName("country")
    @TypeConverters(Converters::class)
    val country: List<EntityValue>,

    @SerializedName("latitude")
    val latitude: Double,

    @SerializedName("longitude")
    val longitude: Double,

    @SerializedName("weatherUrl")
    @TypeConverters(Converters::class)
    val weatherUrl: List<EntityValue>,

    ) : Parcelable {

    // returns 'City, Country'
    override fun toString(): String {
        return areaName[0].value + ", " + country[0].value
    }

}
