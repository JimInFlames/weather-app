package com.example.weathertest.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.weathertest.data.model.Area

@Database(entities = [Area::class], version = 1)
@TypeConverters(Converters::class)
abstract class AreaDatabase : RoomDatabase() {
    abstract fun areaDao(): AreaDao
}