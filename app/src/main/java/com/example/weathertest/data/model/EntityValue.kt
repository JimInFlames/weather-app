package com.example.weathertest.data.model

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class EntityValue(
    @SerializedName("value")
    val value: String,
): Parcelable


