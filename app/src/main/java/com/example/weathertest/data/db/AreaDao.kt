package com.example.weathertest.data.db

import androidx.room.*
import com.example.weathertest.data.model.Area
import kotlinx.coroutines.flow.Flow

@Dao
interface AreaDao {

    @Query("SELECT * FROM area")
    fun getAllAreas(): Flow<List<Area>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAreas(area: List<Area>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(area: Area)

    @Query("DELETE FROM area")
    suspend fun deleteAllAreas()

    @Delete
    suspend fun delete(area: Area)
}