package com.example.weathertest.data.model

import com.google.gson.annotations.SerializedName

data class CurrentCondition(
    @SerializedName("temp_C")
    val tempC: Int,
    @SerializedName("weatherIconUrl")
    val weatherIconUrl: List<EntityValue>,
    @SerializedName("weatherDesc")
    val weatherDesc: List<EntityValue>,
)
