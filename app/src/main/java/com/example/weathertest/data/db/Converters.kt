package com.example.weathertest.data.db

import androidx.room.TypeConverter
import com.example.weathertest.data.model.EntityValue
import com.google.gson.Gson

class Converters {

    @TypeConverter
    fun listToJson(value: List<EntityValue>?): String = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<EntityValue>::class.java).toList()
}