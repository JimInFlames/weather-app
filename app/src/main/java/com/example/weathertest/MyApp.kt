package com.example.weathertest

import android.app.Application
import com.example.weathertest.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        /**
         * Start koin using our modules here
         */
        startKoin {
            androidContext(this@MyApp)
            modules(
                listOf(
                    networkModule, repositoryModule, roomModule,
                    weatherDetailsViewModelModule, homeAreasViewModelModule, searchViewModelModule
                )
            )
        }
    }
}