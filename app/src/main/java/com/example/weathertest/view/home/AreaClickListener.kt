package com.example.weathertest.view.home

import com.example.weathertest.data.model.Area

interface AreaClickListener {
    fun onAreaClicked(item: Area)
    fun onAreaDeleted(item : Area)
}
