package com.example.weathertest.view.details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weathertest.R
import com.example.weathertest.data.model.Weather
import com.example.weathertest.databinding.DayItemLayoutBinding
import com.example.weathertest.util.Utilities.Companion.getNameDay

/**
 * Adapter for the expandable days in the weather details screen
 */
class DaysAdapter(
    var weather: List<Weather>? = null,
) :
    RecyclerView.Adapter<DaysAdapter.DayViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): DayViewHolder {
        val dayItemLayoutBinding =
            DayItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DayViewHolder(dayItemLayoutBinding)
    }

    private lateinit var hourlyAdapter: HourlyAdapter

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        val weatherData = weather?.get(position) // get weather data for current day

        holder.setData(weatherData, position)
        holder.viewBinding.dayName.text = weatherData?.let { getNameDay(it.date) } ?: ""

        // Create Hourly adapter, and attach it to recycler
        hourlyAdapter = HourlyAdapter(emptyList())
        holder.viewBinding.daysRecyclerView.adapter = hourlyAdapter

        // apply hourly data for this day
        weatherData?.let {
            hourlyAdapter.hourlyData = it.hourlyData
        }

        // On root click, expand or collapse the day view
        holder.viewBinding.root.setOnClickListener {
            if (holder.viewBinding.expandableLayout.visibility == View.VISIBLE) {
                holder.viewBinding.expandableLayout.visibility = View.GONE
                holder.viewBinding.arrowButton.setImageResource(R.drawable.ic_baseline_expand_more_24)
            } else {
                holder.viewBinding.expandableLayout.visibility = View.VISIBLE
                holder.viewBinding.arrowButton.setImageResource(R.drawable.ic_baseline_expand_less_24)
            }
        }
    }

    override fun getItemCount(): Int {
        return weather?.size!!
    }

    class DayViewHolder(binding: DayItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var currentCallItem: Weather? = null
        private var currentPosition: Int = 0
        val viewBinding = binding

        fun setData(weather: Weather?, position: Int) {
            this.currentCallItem = weather
            this.currentPosition = position
        }
    }

}