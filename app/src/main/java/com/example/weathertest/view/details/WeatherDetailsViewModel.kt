package com.example.weathertest.view.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weathertest.data.repository.WeatherRepository
import com.example.weathertest.network.response.WeatherApiResultData
import kotlinx.coroutines.launch

class WeatherDetailsViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {

    private val _weatherData: MutableLiveData<WeatherApiResultData> = MutableLiveData()
    var weatherData: LiveData<WeatherApiResultData>
        get() {
            return _weatherData
        }
        set(value) {}

    fun getWeatherDetails(queryString: String) {
        viewModelScope.launch {
            _weatherData.value = weatherRepository.getWeatherDetails(queryString).data
        }
    }
}

