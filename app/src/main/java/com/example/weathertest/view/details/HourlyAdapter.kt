package com.example.weathertest.view.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.weathertest.data.model.HourlyData
import com.example.weathertest.databinding.HourlyItemLayoutBinding
import com.example.weathertest.util.Utilities.Companion.getFormattedHour

/**
 * Adapter for horizontal hour items
 */
class HourlyAdapter(
    var hourlyData: List<HourlyData>? = null,
) :
    RecyclerView.Adapter<HourlyAdapter.DayViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): DayViewHolder {
        val hourlyItemLayoutBinding =
            HourlyItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DayViewHolder(hourlyItemLayoutBinding)
    }

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        val callItem = hourlyData?.get(position)
        holder.setData(callItem, position)

        // Set hourly data
        holder.viewBinding.weatherHour.text = getFormattedHour(callItem?.time.toString())
        holder.viewBinding.weatherHourDesc.text = callItem?.weatherDesc?.get(0)?.value
        holder.viewBinding.weatherHourTempC.text = callItem?.tempC?.toString() + "°C"

        // Glide API icon onto the image
        callItem?.weatherIconUrl?.get(0)?.value.run {
            Glide
                .with(holder.viewBinding.root)
                .load(this)
                .centerCrop()
                .into(holder.viewBinding.weatherImage)
        }
    }

    override fun getItemCount(): Int {
        return hourlyData!!.size
    }

    class DayViewHolder(binding: HourlyItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var currentCallItem: HourlyData? = null
        private var currentPosition: Int = 0
        val viewBinding = binding

        fun setData(hourlyData: HourlyData?, position: Int) {
            this.currentCallItem = hourlyData
            this.currentPosition = position
        }
    }

}