package com.example.weathertest.view.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.weathertest.R
import com.example.weathertest.data.model.Area
import com.example.weathertest.databinding.FragmentWeatherDetailsBinding
import org.koin.android.ext.android.inject

class WeatherDetailsFragment : Fragment() {

    private lateinit var area: Area // Current area being showed in this fragment
    private lateinit var binding: FragmentWeatherDetailsBinding
    private lateinit var adapter: DaysAdapter

    private val viewModel: WeatherDetailsViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // get the area from the bundle/args
        area = WeatherDetailsFragmentArgs.fromBundle(requireArguments()).area

        // try to get the weather data from the API
        viewModel.getWeatherDetails(area.areaName[0].value + "," + area.country[0].value)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentWeatherDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.daysRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter = DaysAdapter(emptyList())
        binding.daysRecyclerView.adapter = adapter

        // observer weather data for changes
        viewModel.weatherData.observe(viewLifecycleOwner, {

            it?.let {

                // populate weather list data data
                it.weather.let { weatherList ->
                    adapter.weather = weatherList
                    adapter.notifyDataSetChanged()
                }

                // set data for current weather conditions
                binding.areaName.text = area.toString()
                binding.tempC.text =
                    resources.getString(R.string.celsius, it.currentCondition?.get(0)!!.tempC)
                binding.weatherDesc.text = it.currentCondition[0].weatherDesc[0].value

                // glide current weather icon onto the view
                it.currentCondition[0].weatherIconUrl[0].value.run {
                    Glide
                        .with(binding.root)
                        .load(this)
                        .centerCrop()
                        .into(binding.weatherImage)
                }

            } ?: run { // Show message on fail
                Toast.makeText(context,
                    getString(R.string.network_error),
                    Toast.LENGTH_LONG).show()
            }
        })
    }
}
