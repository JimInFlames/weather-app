package com.example.weathertest.view.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weathertest.data.model.Area
import com.example.weathertest.data.repository.WeatherRepository
import kotlinx.coroutines.launch

class SearchAreaViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {
    private val _areas: MutableLiveData<List<Area>> = MutableLiveData()
    var areas: LiveData<List<Area>>
        get() {
            return _areas
        }
        set(value) {}

    fun searchBetweenAreas(queryString: String) {
        viewModelScope.launch {
            weatherRepository.searchForAreas(queryString).results?.areaList?.let {
                _areas.value = it
            } ?: run {
                _areas.value = emptyList()
            }
        }
    }

    suspend fun insertArea(area: Area) {
        weatherRepository.insertArea(area)
    }
}