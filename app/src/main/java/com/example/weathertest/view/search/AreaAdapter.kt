package com.example.weathertest.view.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.weathertest.data.model.Area
import com.example.weathertest.databinding.AreaItemLayoutBinding
import com.example.weathertest.view.home.AreaClickListener

/**
 * Using the same area adapter for search, and home screen, passing
 * @param canDelete as an indicator on whether to show the delete button or not
 */
class AreaAdapter(
    var areas: List<Area>,
    private val clickListener: AreaClickListener,
    private var canDelete: Boolean,
) :
    RecyclerView.Adapter<AreaAdapter.SearchViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): SearchViewHolder {
        val searchItemLayoutBinding =
            AreaItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(searchItemLayoutBinding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        val area = areas[position]
        holder.setData(area, position)

        // callback on area clicked
        holder.viewBinding.root.setOnClickListener {
            clickListener.onAreaClicked(area)
        }
        holder.viewBinding.areaName.text = area.toString()

        // show or hide the delete button
        if (canDelete) {
            holder.viewBinding.deleteAreaImage.visibility = View.VISIBLE
            holder.viewBinding.deleteAreaImage.setOnClickListener {
                clickListener.onAreaDeleted(area)
            }
        } else {
            holder.viewBinding.deleteAreaImage.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int {
        return areas.size
    }

    class SearchViewHolder(binding: AreaItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var currentCallItem: Area? = null
        private var currentPosition: Int = 0
        val viewBinding = binding

        fun setData(area: Area?, position: Int) {
            this.currentCallItem = area
            this.currentPosition = position
        }
    }
}