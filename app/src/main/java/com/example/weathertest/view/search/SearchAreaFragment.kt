package com.example.weathertest.view.search

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weathertest.data.model.Area
import com.example.weathertest.databinding.FragmentSearchAreaBinding
import com.example.weathertest.view.home.AreaClickListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class SearchAreaFragment : Fragment(), AreaClickListener {

    private val viewModel: SearchAreaViewModel by inject()
    private lateinit var binding: FragmentSearchAreaBinding
    private lateinit var adapter: AreaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * Notify data set whenever this list changes
         */
        viewModel.areas.observe(this, {
            adapter.areas = it
            adapter.notifyDataSetChanged()
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentSearchAreaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerViewSearchResult.layoutManager = LinearLayoutManager(context)
        adapter = AreaAdapter(emptyList(), this, false)
        binding.recyclerViewSearchResult.adapter = adapter

        autoOpenKeyboard()

        binding.toolbarSearch.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty())
                    viewModel.searchBetweenAreas(s.toString())
                if (s != null) {
                    if (s.isEmpty())
                        adapter.areas = emptyList()
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

    }

    /**
     * When fragment view initializes, auto open keyboard
     */
    private fun autoOpenKeyboard() {
        binding.toolbarSearch.editTextSearch.requestFocus()
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(binding.toolbarSearch.editTextSearch, InputMethodManager.SHOW_IMPLICIT)
    }

    /**
     * On area click, attempt to save it in the database, and navigate to home screen
     */
    override fun onAreaClicked(item: Area) {
        CoroutineScope(IO).launch {
            viewModel.insertArea(item)
        }
        Navigation.findNavController(binding.root)
            .navigate(SearchAreaFragmentDirections.actionSearchFragmentToHomeFragment())
    }

    /**
     * Button is deactivated for this case on search screen
     */
    override fun onAreaDeleted(item: Area) {

    }
}