package com.example.weathertest.view.home

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.weathertest.R
import com.example.weathertest.data.model.Area
import com.example.weathertest.databinding.FragmentHomeAreasBinding
import com.example.weathertest.view.search.AreaAdapter
import org.koin.android.ext.android.inject

class HomeAreasFragment : Fragment(), AreaClickListener {

    private val viewModel: HomeAreasViewModel by inject()
    private lateinit var adapter: AreaAdapter
    private lateinit var binding: FragmentHomeAreasBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.getStoredAreas() // First fetch of the areas from the database
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHomeAreasBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerHomeAreas.layoutManager = LinearLayoutManager(context)
        adapter = AreaAdapter(emptyList(), this, true)
        binding.recyclerHomeAreas.adapter = adapter

        // Observer areas, and update on any list change
        viewModel.areas.observe(viewLifecycleOwner, {
            if (it.isEmpty())
                binding.emptyListMessage.visibility = View.VISIBLE
            else
                binding.emptyListMessage.visibility = View.GONE

            adapter.areas = it
            adapter.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_activity_menu, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    /**\
     * Handle menu clicks
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            // navigate to search fragment
            R.id.app_bar_search -> view?.let {
                Navigation.findNavController(it)
                    .navigate(HomeAreasFragmentDirections.actionHomeFragmentToSearchFragment())
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    /**
     * On area click, navigate to details fragment passing the area data
     */
    override fun onAreaClicked(item: Area) {
        Navigation.findNavController(binding.root)
            .navigate(HomeAreasFragmentDirections.actionHomeFragmentToDetailsFragment(item))
    }

    /**
     * Delete area from the database
     */
    override fun onAreaDeleted(item: Area) {
        viewModel.deleteArea(item)
    }
}
