package com.example.weathertest.view.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.weathertest.data.model.Area
import com.example.weathertest.data.repository.WeatherRepository
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class HomeAreasViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {
    private val _areas: MutableLiveData<List<Area>> = MutableLiveData()
    var areas: LiveData<List<Area>>
        get() {
            return _areas
        }
        set(value) {}

    fun getStoredAreas() {
        viewModelScope.launch {
            weatherRepository.getStoredAreas().collect { items ->
                _areas.postValue(items)
            }
        }
    }

    fun deleteArea(area: Area) {
        viewModelScope.launch {
            weatherRepository.deleteArea(area)
        }
    }
}