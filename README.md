# Weather App #

This app communicates with the World Weather Online API (https://www.worldweatheronline.com/developer/api/) to fetch
current / future weather date about a specific area.
The user can add or remove an area from the search menu, and then navigate to the area to see the weather forecast.
The areas persist offline.

### Why certain approaches were used, why others were not used, design patterns ###
Used for this project:

* MVVM architecure

For seperation of concern, seperating the view, from the viewmodel and the model, making it easier
to expand, add / remove new features and unit test.

* Viewmodels

To make sure our data survive orientation changes, and remove logic from the UI


### Why certain libraries were used, if any ###

* Koin

Used for dependency injection, to easily inject modules into the areas needed

* Retrofit / Gson

For server communication / data parsing / serialize

* Room

For SQLite support, saving and fetching the areas

* Navigation

To traverse between the fragments, passing data as args where needed

* Glide

To fetch / cache images from the API

* Lifecycle

Using LiveData to keep all the values observed / updated whenever a change occurs


### Anything extra you would have done given more time ###

Would finish up unit-instrumented tests, add more state logic (Loading indicators, error messages etc),
add animated weather effects depending on the current weather, add more weather data maybe, and polish more
